# 类似飞书OKR输入框的AT提及功能

### 介绍
div模拟富文本编辑框，借助 tributejs 实现，里面有可交互的 @ 提及功能。

Demo地址：https://zhangxinxu.gitee.io/okr-at-mention/


### 使用说明

1.  引入 src 目录中的 JS 和 CSS
2.  按照语法进行参数设置和处理

```html
<link rel="stylesheet" href="./src/atMention.css">

<div id="container"></div>

<script type="module">
    import atWakaka from './src/atWakaka.js';
    atWakaka('container', {
        url: './cgi/data.json'
    });
</script>
```

### 语法和参数

语法为：

```js
atWakaka(container, options, optionsTribute);
```

其中：

<dl>
<dt>container</dt>
<dd>可输入的编辑框容器元素，可以是 DOM 元素本身，也可以是元素的 id 字符串。</dd>
<dt>options</dt>
<dd>可选参数，下面有注释说明。</dd>
<dt>optionsTribute</dt>
<dd>可选参数。参见 https://github.com/zurb/tribute 中的参数设置。</dd>
</dl>

#### 关于options可选参数

```js
{
    url: '',
    // 按下回车键后，如果希望阻止默认的回车换行
    // 并做一些事情，这个参数就可以用到
    pressEnter: null,
    // 鼠标经过的提示元素，默认本组件会自己创建
    // 也可以可以自己指定具体的元素
    popOver: 'auto',
    // 鼠标经过和移出 @ 元素的处理
    // event 是事件对象
    // data 是 @元素 对应的请求数据
    // popover 是浮层元素
    onMouseOver: function (event, data, popover) {},
    onMouseOut: function (event, data, popover) {}
}
```

#### 关于 optionsTribute 可选参数

如果 @ 下拉列表样式不满意，或者请求的数据结构和自己这边的不符合，均是使用 optionsTribute 可选参数进行处理。

请求数据处理使用 values 参数（Function 类型），列表模板和选择后的样式使用参数 menuItemTemplate 和 selectTemplate，详见 https://github.com/zurb/tribute 中的参数设置。

或者参见 atWakaka.js 中的使用示意，对其进行一一替换即可。


### 其他

原生 JS 开发，兼容各大框架，但是不兼容 IE 浏览器。